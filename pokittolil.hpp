#ifndef POKITTO_LIL_H
#define POKITTO_LIL_H

/**
  Pokitto LIL interpreter.

  author: Miloslav Ciz AKA drummyfish

  author of the LIL source code (lil.c and lil.h) is Kostas Michalopoulos.
  These files are released under its own free license, found within the
  files.

  This file (pokittolil.hpp) and other ones written solely by drummyfish are
  released under CC0 1.0.

  --------------------------------------------------------------------------

  Manual:

    The screen is persistent during LIL execute! All Pokitto state will be
    restored after the execute.

  LIL functions for Pokitto:

    p_write what [x] [y] [color]       write value to screen
    p_read                             waits for pokitto button press and returns
                                       its value (ABCURDL = 0123456)
    p_pressed button                   returns 1 if given button is being held,
                                       otherwise a false value (codes are same
                                       as for p_read)
    p_clear [color]                    clear screen with given color
    p_wait n                           wait (sleep) for n frames
    p_rect x y w h color [filled]      draw a rectangle
    p_circle x y d color [filled]      draw a circle
    p_line x1 y1 x2 y2 color           draw a line
    p_pixel x y color                  draw a pixel
    p_update                           wait for the next frame, makes drawn graphic appear
    p_frame                            return Pokitto frame number
    p_time                             return elapsed time in ms
    p_rand                             return random byte value (0 - 255)
    p_sin x                            return sin of integer x (percents of a full circle, max = 100) as
                                       a value from -100 to 100

  general LIL cheatsheet (find more in lil's readme.txt)
  
    Everything is a string.
    case-sensitive
    separate with ; or newlines

    ## comment ##
    [...]                        execute what's inside square brackets and substitute the result, e.g.: print [expr 1 + 1]   
    if ["not"] val then [else]   branching, e.g.: if 1 {print one} {print noone}, returns evaluated branch result
    while ["not"] val code       while loop
    for init expr step code      for loop
    foreach [name] l code        executes code for each list item (its value will be in "name"), returns list of evaluations,
                                 e.g.: foreach i [list a b c] {print $i}
    return [val]                 returns from function with optional value

    set var val                  set given variable to given value
    $var                         get variable value (same as set with only one argument)
    expr a b c ...               combines arguments into one math epression, evaluates and returns the result
    func fname args body         define a new function, e.g.: func myfunc {a b} {print $a $b}
    list a b c ...               returns a new list with arguments as items
    count l                      returns number of items in a LIL list
    index l i                    returns ith item in a LIL list (0-based)
    indexof l v                  returns index of first occurence of value in a LIL list, or empty string
    append l v                   appends a value to a LIL list
    slice l i1 [i2]              returns a slice of a LIL list
    eval a b c ...               combines argument into a single string and evaluates as LIL code
    char n                       converts int to char (one-character string)
    rand                         returns a random number between 0.0 and 1.0
    inc name [val]               increment variable (or add given value)
*/

extern "C"
{
  #include "lil.h"
}

const char ERR_PARAM_COUNT[] = "Not enough function parameters.";
const char ERR_PARAM_VALUE[] = "Wrong function parameter value.";
const uint8_t POKITTO_BUTTONS[] =
  {BTN_A, BTN_B, BTN_C, BTN_UP, BTN_RIGHT, BTN_DOWN, BTN_LEFT};

const int8_t SIN_TABLE[] =
  {
    0,  6,  12, 18, 24, 30, 36,  42, 48, 53, 58, 63, 68, 72, 77, 80, 84, 87, 90,
    92, 95, 96, 98, 99, 99, 100, 99, 99, 98, 96, 95, 92, 90, 87, 84, 80, 77, 72,
    68, 63, 58, 53, 48, 42, 36,  30, 24, 18, 12, 6, 0, -6, -12, -18, -24, -30,
   -36, -42, -48, -53, -58, -63, -68, -72, -77, -80, -84, -87, -90, -92, -95,
   -96, -98, -99, -99, -100, -99, -99, -98, -96, -95, -92, -90, -87, -84, -80,
   -77, -72, -68, -63, -58, -53, -48, -42, -36, -30, -24, -18, -12, -6
  };

#define p Pokitto::Core

void waitForPokitto()
{
  while (p::isRunning())
    if (p::update())
      break;
}

static LILCALLBACK lil_value_t fnc_update(lil_t lil, size_t argc, lil_value_t* argv)
{
  waitForPokitto();
  return NULL;
}

static LILCALLBACK lil_value_t fnc_rect(lil_t lil, size_t argc, lil_value_t* argv)
{
  if (argc < 5)
  {
    lil_set_error(lil,ERR_PARAM_COUNT);
    return NULL;
  }

  p::display.setColor(lil_to_integer(argv[4]));

  if (argc > 5 && lil_to_integer(argv[5]))
    p::display.fillRect(lil_to_integer(argv[0]),lil_to_integer(argv[1]),lil_to_integer(argv[2]),lil_to_integer(argv[3]));
  else
    p::display.drawRect(lil_to_integer(argv[0]),lil_to_integer(argv[1]),lil_to_integer(argv[2]),lil_to_integer(argv[3]));

  return NULL;
}

static LILCALLBACK lil_value_t fnc_pixel(lil_t lil, size_t argc, lil_value_t* argv)
{
  if (argc < 3)
  {
    lil_set_error(lil,ERR_PARAM_COUNT);
    return NULL;
  }

  p::display.setColor(lil_to_integer(argv[2]));
  p::display.drawPixel(lil_to_integer(argv[0]),lil_to_integer(argv[1]));

  return NULL;
}

static LILCALLBACK lil_value_t fnc_circle(lil_t lil, size_t argc, lil_value_t* argv)
{
  if (argc < 4)
  {
    lil_set_error(lil,ERR_PARAM_COUNT);
    return NULL;
  }

  p::display.setColor(lil_to_integer(argv[3]));

  if (argc > 4 && lil_to_integer(argv[4]))
    p::display.fillCircle(lil_to_integer(argv[0]),lil_to_integer(argv[1]),lil_to_integer(argv[2]));
  else
    p::display.drawCircle(lil_to_integer(argv[0]),lil_to_integer(argv[1]),lil_to_integer(argv[2]));

  return NULL;
}

static LILCALLBACK lil_value_t fnc_line(lil_t lil, size_t argc, lil_value_t* argv)
{
  if (argc < 5)
  {
    lil_set_error(lil,ERR_PARAM_COUNT);
    return NULL;
  }

  p::display.setColor(lil_to_integer(argv[4]));

  p::display.drawLine(lil_to_integer(argv[0]),lil_to_integer(argv[1]),lil_to_integer(argv[2]),lil_to_integer(argv[3]));

  return NULL;
}

static LILCALLBACK lil_value_t fnc_write(lil_t lil, size_t argc, lil_value_t* argv)
{
  if (argc < 1)
  {
    lil_set_error(lil,ERR_PARAM_COUNT);
    return NULL;
  }

  if (argc >= 3)
    p::display.setCursor(lil_to_integer(argv[1]),lil_to_integer(argv[2]));
  else
    p::display.setCursor(1,1);

  unsigned char colorBackup = p::display.invisiblecolor;

  unsigned char color = argc >= 4 ? lil_to_integer(argv[3]) : 1;

  p::display.invisiblecolor = color + 1;
  p::display.bgcolor = color + 1;

  p::display.setColor(color);
  p::display.print((char *) lil_to_string(argv[0]));

  p::display.invisiblecolor = colorBackup;

  return NULL;
}

static LILCALLBACK lil_value_t fnc_read(lil_t lil, size_t argc, lil_value_t* argv)
{
  int8_t result = -1;

  while (p::isRunning())
  {
    if (p::update())
    {
      if (p::buttons.pressed(BTN_A))
        result = 0;
      else if (p::buttons.pressed(BTN_B))
        result = 1;
      else if (p::buttons.pressed(BTN_C))
        result = 2;
      else if (p::buttons.pressed(BTN_UP))
        result = 3;
      else if (p::buttons.pressed(BTN_RIGHT))
        result = 4;
      else if (p::buttons.pressed(BTN_DOWN))
        result = 5;
      else if (p::buttons.pressed(BTN_LEFT))
        result = 6;

      if (result > -1)
        break;
    }
  }

  return lil_alloc_integer(result);
}

static LILCALLBACK lil_value_t fnc_frame(lil_t lil, size_t argc, lil_value_t* argv)
{
  return lil_alloc_integer(p::frameCount);
}

static LILCALLBACK lil_value_t fnc_time(lil_t lil, size_t argc, lil_value_t* argv)
{
  return lil_alloc_integer(p::getTime());
}

static LILCALLBACK lil_value_t fnc_pressed(lil_t lil, size_t argc, lil_value_t* argv)
{
  if (argc < 1)
  {
    lil_set_error(lil,ERR_PARAM_COUNT);
    return NULL;
  }

  int but = lil_to_integer(argv[0]);

  if (but < 0 || but > 6)
  {
    lil_set_error(lil,ERR_PARAM_VALUE);
    return NULL;
  }

  if (p::buttons.timeHeld(POKITTO_BUTTONS[but]) > 0)
    return lil_alloc_integer(1);

  return NULL;
}

static LILCALLBACK lil_value_t fnc_rand(lil_t lil, size_t argc, lil_value_t* argv)
{
  return lil_alloc_integer(rand() % 256);
}

static LILCALLBACK lil_value_t fnc_clear(lil_t lil, size_t argc, lil_value_t* argv)
{
  p::display.bgcolor = argc > 0 ? lil_to_integer(argv[0]) : 0;
  p::display.clear();

  return NULL;
}

static LILCALLBACK lil_value_t fnc_wait(lil_t lil, size_t argc, lil_value_t* argv)
{
  if (argc < 1)
  {
    lil_set_error(lil,ERR_PARAM_COUNT);
    return NULL;
  }

  uint16_t frameEnd = p::frameCount + lil_to_integer(argv[0]);

  while (p::isRunning())
    if (p::update())
    {
      if (p::frameCount > frameEnd)
        break;
    }

  return NULL;
}

static LILCALLBACK lil_value_t fnc_sin(lil_t lil, size_t argc, lil_value_t* argv)
{
  if (argc < 1)
  {
    lil_set_error(lil,ERR_PARAM_COUNT);
    return NULL;
  }

  int value = lil_to_integer(argv[0]);
 
  value = value >= 0 ? (value % 100) : (100 + (value % 100) - 1);

  return lil_alloc_integer(SIN_TABLE[value]);
}

class LILInterpreter
{
public:
  lil_t lil;

  struct RunResult
  {
    bool ok;
    const char *message;
    size_t position;       // local position of the error

    RunResult()
    {
      this->ok = true;
      this->message = 0;
      this->position = 0;
    }
  };

  LILInterpreter()
  {
    this->lil = lil_new();
  }

  void reset()
  {
    lil_free(this->lil);
    this->lil = lil_new();
    lil_register(lil,"p_write",fnc_write);
    lil_register(lil,"p_read",fnc_read);
    lil_register(lil,"p_clear",fnc_clear);
    lil_register(lil,"p_rect",fnc_rect);
    lil_register(lil,"p_pixel",fnc_pixel);
    lil_register(lil,"p_circle",fnc_circle);
    lil_register(lil,"p_line",fnc_line);
    lil_register(lil,"p_wait",fnc_wait);
    lil_register(lil,"p_update",fnc_update);
    lil_register(lil,"p_frame",fnc_frame);
    lil_register(lil,"p_time",fnc_time);
    lil_register(lil,"p_rand",fnc_rand);
    lil_register(lil,"p_pressed",fnc_pressed);
    lil_register(lil,"p_sin",fnc_sin);
  }

  ~LILInterpreter()
  {
    lil_free(this->lil);
  }

  RunResult runCode(const char *code)
  {
    this->reset();

    srand(p::getTime());

    RunResult result;

    bool persistenceBackup = p::display.persistence;
    unsigned char colorBackup = p::display.color;
    unsigned char backgroundBackup = p::display.bgcolor;
    const unsigned char *fontBackup = p::display.font;

    p::display.setFont(fontTiny);

    p::display.persistence = true;

    while (p::isRunning())
    {
      if (p::update())
      {
        p::display.bgcolor = 0;
        p::display.setColor(1);
        p::display.clear();
        break;
      }
    }

    lil_value_t r;
    r = lil_parse(this->lil,code,0,0);
   
    lil_free_value(r);

    size_t pos;
    const char *msg;

    if (lil_error(this->lil, &msg, &pos))
    {
      result.ok = false;
      result.message = msg;
      result.position = pos;

      // printf("LIL ERROR: %s\n",msg);
    }
    
    p::display.setFont(fontBackup);
    p::display.color = colorBackup;
    p::display.bgcolor = backgroundBackup;
    p::display.persistence = persistenceBackup;

    return result;
  }
};

#undef p

#endif
